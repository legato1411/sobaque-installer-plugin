<?php

namespace sobaque\Composer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class SobaqueInstaller extends LibraryInstaller
{
    public $supportedTypes = [
        'sobaque-module' => 'Modules',
        'sobaque-theme' => 'Themes'
    ];

    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        if (preg_match('/sobaque\/([\w]+)-([\w]+)/', $package->getPrettyName(), $mch)) {
            $type = 'sobaque-' . $mch[2];
            if (array_key_exists($type, $this->supportedTypes)) {
                return $this->supportedTypes[$type] . '/' . ucfirst($mch[1]);
            }
        }

        throw new \InvalidArgumentException('Unable to install Sobaque extension. Unknown extension type.');
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return array_key_exists($packageType, $this->supportedTypes);
    }
}
